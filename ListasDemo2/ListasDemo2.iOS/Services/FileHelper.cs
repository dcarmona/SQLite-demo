﻿using System;
using Xamarin.Forms;
using System.IO;
using ListasDemo2.iOS.Services;
using ListasDemo2.Services;

[assembly: Dependency(typeof(FileHelper))]
namespace ListasDemo2.iOS.Services
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string fileName)
        {
            string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libFolder = Path.Combine(docFolder, "..", "Databases");
            if (!Directory.Exists(libFolder))
            {
                Directory.CreateDirectory(libFolder);
            }

            return Path.Combine(libFolder, fileName);
        }
    }
}