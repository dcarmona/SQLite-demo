﻿using ListasDemo2.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ListasDemo2.ViewModel
{
    public class FriendViewViewModel
    {
        public Command SaveFriendCommand { get; set; }
        public Friend NewFriend { get; set; }
        public INavigation Navigation { get; set; }

        public FriendViewViewModel(INavigation navigation)
        {
            NewFriend = new Friend();
            SaveFriendCommand = new Command(async() => await SaveFriend());
            Navigation = navigation;
        }

        public FriendViewViewModel(INavigation navigation, Friend friend)
        {
            NewFriend = friend;
            SaveFriendCommand = new Command(async () => await SaveFriend());
            Navigation = navigation;
        }

        private async Task SaveFriend()
        {
            await App.Database.SaveItemAsync(NewFriend);
            await Navigation.PopToRootAsync();
        }
    }
}
