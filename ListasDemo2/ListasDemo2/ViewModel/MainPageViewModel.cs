﻿using ListasDemo2.Helpers;
using ListasDemo2.Model;
using ListasDemo2.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ListasDemo2.ViewModel
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        public ObservableCollection<Grouping<string, Friend>> Friends { get; set; }
        public Friend CurrentFriend { get => currentFriend; set { currentFriend = value; OnPropertyChanged();  } }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public Command AddFriendCommand { get; set; }
        private INavigation Navigation;
        private Friend currentFriend;

        

        public ICommand ItemTappedCommand { get; set; }

        public MainPageViewModel(INavigation navigation)
        {
            FriendRepository repository
                = new FriendRepository();
            Friends = repository.GetAllGrouped();
            Navigation = navigation;
            AddFriendCommand = new Command(async () => await NavigateToFriendView());
            ItemTappedCommand = new Command(async () => await NavigateToEditFriendView());
        }

        private async Task NavigateToEditFriendView()
        {
            await Navigation.PushAsync(new FriendView(currentFriend));
        }

        private async Task NavigateToFriendView()
        {
            await Navigation.PushAsync(new FriendView());
        }
    }
}
